import subprocess
import optparse
import re

def change_mac(interface,new_mac):
    print('\n')
    print('Changing MAC of ' + interface + ' to ' + new_mac)
    print('\n')

    # Run CLI calls
    subprocess.call(['ifconfig',interface,'down'])
    subprocess.call(['ifconfig',interface,'hw','ether',new_mac])
    subprocess.call(['ifconfig',interface,'up'])


def handle_arguments():
    # Initiate parser
    parser = optparse.OptionParser()

    # Add argument options
    parser.add_option("-i","--interface",dest="interface",help="Enter name of interface")
    parser.add_option("-m","--mac",dest="new_mac",help="Enter new mac address")

    # Parse entered arguments
    (values, arguments) = parser.parse_args()

    if not values.interface:
        parser.error("[-] Please specify an interface with argument -i, use --help for more info.")
    if not values.new_mac:
        parser.error("[-] Please specify a valid mac address with argument -m, use --help for more info.")

    return values


def get_mac(interface):
    ifconfig_result = subprocess.check_output(["ifconfig",interface])
    search_result = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w",ifconfig_result)

    if search_result:
        search_mac = search_result.group(0)
    else:
        print('No MAC address found. Exiting program.')
        quit()

    return search_mac



def check_result(interface,new_mac):
    print('Validating result...')
    search_mac = get_mac(interface)

    # Check if MAC is what the user requested
    if search_mac == new_mac:
        print('[+] MAC address successfully updated!')
    else: 
        print('[-] MAC address update failed')

    

###### MAIN CODE #######
# Handle arguments
values = handle_arguments()

# Print startup 
current_mac = get_mac(values.interface)
print('Welcome to MACchanger')
print('\n')
print('The current MAC address of ' + values.interface + ' is: ' + current_mac)

# Change MAC address 
change_mac(values.interface,values.new_mac)

# Check & Validate result
check_result(values.interface,values.new_mac)